Source: slingshot
Section: games
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ryan Kavanagh <rak@debian.org>
Build-Depends: debhelper-compat (= 9),
 python (>= 2.6.6-3~)
Standards-Version: 3.9.8
Homepage: http://github.com/ryanakca/slingshot
Vcs-Git: https://salsa.debian.org/python-team/packages/slingshot.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/slingshot

Package: slingshot
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
 , fonts-freefont-ttf
 , python-pygame (>= 1.7.1)
Description: simple 2D shooting strategy game set in space, with gravity
 Slingshot is a two dimensional, turn based simulation-strategy game
 set in the gravity fields of several planets. It is a highly
 addictive game, and never the same from round to round due to its
 randomly generated playing fields.
 .
 It is a deceptively simple game, the goal is to shoot the other
 spacecraft through the field of planets, but their gravity makes it
 tricky. The effects of the gravity mean that although it is easy to
 learn how to play, and to enjoy playing, it could take a lifetime to
 thoroughly master.
